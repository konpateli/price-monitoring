package gr.pateli.pricemonitoringback.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ResponseDto<T> {
    private String successMessage;
    private String error;
    private T data;
}


