package gr.pateli.pricemonitoringback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordResetFormDto {
    private String jwt;
    private String password;
    private String repeatedPassword;
}
