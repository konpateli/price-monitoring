package gr.pateli.pricemonitoringback.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MonitorListUpdateFormDto {
    private Long id;
    private String name;
    private Set<Long> rooms;
}