package gr.pateli.pricemonitoringback.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoomPriceRequestDto {
    private Long id;
    private Integer size;
    private Integer page;
    private List<String> sort;
}
