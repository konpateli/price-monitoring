package gr.pateli.pricemonitoringback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelInfoRequestDto {
    private String link;
}
