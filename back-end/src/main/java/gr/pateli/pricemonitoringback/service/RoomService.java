package gr.pateli.pricemonitoringback.service;

import gr.pateli.pricemonitoringback.dto.ResponseDto;
import gr.pateli.pricemonitoringback.dto.RoomPriceRequestDto;
import gr.pateli.pricemonitoringback.utils.UrlConstants;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class RoomService {

    private final HttpService httpService;
    private final Map<String, String> headerMap = new HashMap<>();

    public ResponseEntity<ResponseDto<String>> getRoomHistory(Long id, HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpGetRequest(UrlConstants.roomHistoryUrl + "/" + id, headerMap);
    }

    public ResponseEntity<ResponseDto<String>> getRoomPrice(RoomPriceRequestDto roomPriceRequestDto,
                                                            HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        String url = UrlConstants.roomPriceUrl + "/" + roomPriceRequestDto.getId();
        if (roomPriceRequestDto.getSize() != null) {
            url = url + "?size=" + roomPriceRequestDto.getSize();
        }
        if (roomPriceRequestDto.getPage() != null) {
            url = url + "?page=" + roomPriceRequestDto.getPage();
        }
//        if (roomPriceRequestDto.getSort() != null) {
//            url = url + "?page=" + roomPriceRequestDto.getPage();
//        }
        return this.httpService.httpGetRequest(url, headerMap);
    }
}
