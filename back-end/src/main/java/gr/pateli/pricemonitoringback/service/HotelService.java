package gr.pateli.pricemonitoringback.service;

import gr.pateli.pricemonitoringback.dto.HotelInfoRequestDto;
import gr.pateli.pricemonitoringback.dto.ResponseDto;
import gr.pateli.pricemonitoringback.utils.UrlConstants;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class HotelService {

    private final HttpService httpService;
    private final Map<String, String> headerMap = new HashMap<>();

    public ResponseEntity<ResponseDto<String>> getHotelInfo(HotelInfoRequestDto hotelInfoRequestDto
            , HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpPostRequest(UrlConstants.hotelInfoUrl, hotelInfoRequestDto, headerMap);
    }

    public ResponseEntity<ResponseDto<String>> getHotel(Long id, HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpGetRequest(UrlConstants.hotelUrl + "/" + id, headerMap);
    }
}
