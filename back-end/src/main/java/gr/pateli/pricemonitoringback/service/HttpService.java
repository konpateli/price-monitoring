package gr.pateli.pricemonitoringback.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import gr.pateli.pricemonitoringback.dto.ResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class HttpService {

    public ResponseEntity<ResponseDto<String>> httpGetRequest(String url, Map<String, String> headers) {
        try {
            HttpRequest.Builder requestBuilder = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .GET();
            return getResponseDtoResponseEntity(headers, requestBuilder);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public ResponseEntity<ResponseDto<String>> httpPostRequest(String url, Object object, Map<String, String> headers) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper.writeValueAsString(object);
            HttpRequest.Builder requestBuilder = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody));
            return getResponseDtoResponseEntity(headers, requestBuilder);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage());
            return null;
        }
    }

    public ResponseEntity<ResponseDto<String>> httpPutRequest(String url, Object object, Map<String, String> headers) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper.writeValueAsString(object);
            HttpRequest.Builder requestBuilder = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .PUT(HttpRequest.BodyPublishers.ofString(requestBody));
            return getResponseDtoResponseEntity(headers, requestBuilder);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public ResponseEntity<ResponseDto<String>> httpDeleteRequest(String url, Map<String, String> headers) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            HttpRequest.Builder requestBuilder = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .DELETE();
            return getResponseDtoResponseEntity(headers, requestBuilder);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    private ResponseEntity<ResponseDto<String>> getResponseDtoResponseEntity(Map<String, String> headers, HttpRequest.Builder requestBuilder) throws IOException, InterruptedException {
        setHeaders(headers, requestBuilder);
        HttpRequest request = requestBuilder.build();
        HttpResponse<String> response = getResponse(request);
        ResponseDto<String> responseDto;
        if (response.statusCode() != 200) {
            responseDto = ResponseDto.<String>builder().error(response.body()).build();
        } else {
            responseDto = ResponseDto.<String>builder()
                    .data(response.body())
                    .build();
        }
        return ResponseEntity.status(response.statusCode()).body(responseDto);
    }

    private static void setHeaders(Map<String, String> headers, HttpRequest.Builder requestBuilder) {
        if (headers != null) {
            headers.forEach(requestBuilder::header);
        }
    }

    private HttpResponse<String> getResponse(HttpRequest request) throws IOException, InterruptedException {
        return HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
    }
}
