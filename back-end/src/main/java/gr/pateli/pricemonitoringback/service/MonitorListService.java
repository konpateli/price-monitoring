package gr.pateli.pricemonitoringback.service;

import gr.pateli.pricemonitoringback.dto.MonitorListCreateRequestDto;
import gr.pateli.pricemonitoringback.dto.MonitorListUpdateFormDto;
import gr.pateli.pricemonitoringback.dto.ResponseDto;
import gr.pateli.pricemonitoringback.utils.UrlConstants;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class MonitorListService {

    private final HttpService httpService;
    private final Map<String, String> headerMap = new HashMap<>();

    public ResponseEntity<ResponseDto<String>> getAllMonitorLists(HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpGetRequest(UrlConstants.monitorListUrl, headerMap);
    }

    public ResponseEntity<ResponseDto<String>> getMonitorList(Long id, HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpGetRequest(UrlConstants.monitorListUrl + "/" + id, headerMap);
    }

    public ResponseEntity<ResponseDto<String>> saveMonitorList(MonitorListCreateRequestDto monitorListCreateRequestDto
            , HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpPostRequest(UrlConstants.monitorListUrl, monitorListCreateRequestDto, headerMap);
    }

    public ResponseEntity<ResponseDto<String>> updateMonitorList(MonitorListUpdateFormDto monitorListUpdateFormDto
            , HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpPutRequest(UrlConstants.monitorListUrl, monitorListUpdateFormDto, headerMap);
    }

    public ResponseEntity<ResponseDto<String>> deleteMonitorList(Long id
            , HttpServletRequest request) {
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", request.getHeader("Authorization"));
        return this.httpService.httpDeleteRequest(UrlConstants.monitorListUrl + "/" + id, headerMap);
    }
}
