package gr.pateli.pricemonitoringback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class PriceMonitoringBackApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PriceMonitoringBackApplication.class, args);
    }

}
