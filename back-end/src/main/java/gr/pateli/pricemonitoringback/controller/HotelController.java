package gr.pateli.pricemonitoringback.controller;

import gr.pateli.pricemonitoringback.dto.HotelInfoRequestDto;
import gr.pateli.pricemonitoringback.dto.ResponseDto;
import gr.pateli.pricemonitoringback.service.HotelService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/hotel")
@CrossOrigin
public class HotelController {

    private final HotelService hotelService;

    @PostMapping("/info")
    public ResponseEntity<ResponseDto<String>> getHotelInfo(@RequestBody HotelInfoRequestDto hotelInfoRequestDto,
                                                            HttpServletRequest request) {
        return this.hotelService.getHotelInfo(hotelInfoRequestDto, request);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<String>> getHotel(@PathVariable Long id,
                                                            HttpServletRequest request) {
        return this.hotelService.getHotel(id, request);
    }
}
