package gr.pateli.pricemonitoringback.controller;

import gr.pateli.pricemonitoringback.dto.ResponseDto;
import gr.pateli.pricemonitoringback.dto.RoomPriceRequestDto;
import gr.pateli.pricemonitoringback.service.RoomService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/room")
@CrossOrigin
public class RoomController {

    private final RoomService roomService;

    @GetMapping("/history/{id}")
    public ResponseEntity<ResponseDto<String>> getRoomHistory(@PathVariable Long id, HttpServletRequest request) {
        return this.roomService.getRoomHistory(id, request);
    }

    @PostMapping("/prices")
    public ResponseEntity<ResponseDto<String>> getRoomPrice(@RequestBody RoomPriceRequestDto roomPriceRequestDto,
                                                            HttpServletRequest request) {
        return this.roomService.getRoomPrice(roomPriceRequestDto, request);
    }
}
