package gr.pateli.pricemonitoringback.controller;

import gr.pateli.pricemonitoringback.dto.MonitorListCreateRequestDto;
import gr.pateli.pricemonitoringback.dto.MonitorListUpdateFormDto;
import gr.pateli.pricemonitoringback.dto.ResponseDto;
import gr.pateli.pricemonitoringback.service.MonitorListService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/monitor-list")
@CrossOrigin
public class MonitorListController {

    private final MonitorListService monitorListService;

    @GetMapping()
    public ResponseEntity<ResponseDto<String>> getAllMonitorLists(HttpServletRequest request) {
        return this.monitorListService.getAllMonitorLists(request);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<String>> getMonitorList(@PathVariable Long id,
                                                              HttpServletRequest request) {
        return this.monitorListService.getMonitorList(id, request);
    }

    @PostMapping()
    public ResponseEntity<ResponseDto<String>> saveMonitorList(@RequestBody MonitorListCreateRequestDto monitorListCreateRequestDto,
                                                               HttpServletRequest request) {
        return this.monitorListService.saveMonitorList(monitorListCreateRequestDto, request);
    }

    @PutMapping()
    public ResponseEntity<ResponseDto<String>> updateMonitorList(@RequestBody MonitorListUpdateFormDto monitorListUpdateFormDto,
                                                               HttpServletRequest request) {
        return this.monitorListService.updateMonitorList(monitorListUpdateFormDto, request);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<String>> deleteMonitorList(@PathVariable Long id,
                                                                 HttpServletRequest request) {
        return this.monitorListService.deleteMonitorList(id, request);
    }
}


