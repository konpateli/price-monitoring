package gr.pateli.pricemonitoringback.utils;

public final class UrlConstants {
    public static final String monitorListUrl = "http://195.251.123.174/monitor_list";
    public static final String hotelInfoUrl = "http://195.251.123.174/hotel/info";
    public static final String hotelUrl = "http://195.251.123.174/hotel";
    public static final String roomHistoryUrl = "http://195.251.123.174/room/history";
    public static final String roomPriceUrl = "http://195.251.123.174/room/price";
}
