const express = require('express');
const axios = require('axios');
const cheerio = require('cheerio');
const cors = require('cors');
const Joi = require('joi');
const UserAgent = require("user-agents")
const _ = require("lodash");

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  optionsSuccessStatus: 204,
}));
app.post('/autocomplete', async (payload, res) => {
  try {
    const response = await axios.post('https://accommodations.booking.com/autocomplete.json', payload.body, {
      headers: {
        'User-Agent': 'Your Customer Agent',
        'Content-Type': 'application/json',
      }
    });
    res.json(response.data);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
});

app.post('/search-results', async (payload, res) => {
  try {
    const {ss, destId, destType} = validateScrapperTaskPayload(payload.body);

    const link = 'https://www.booking.com/searchresults.en-gb.html';
    const url = new URL(link);

    const currentDate = new Date();
    const checkIn = currentDate.toLocaleDateString();
    currentDate.setDate(currentDate.getDate() + 1);
    currentDate.setHours(0, 0, 0, 0);
    const checkOut = currentDate.toLocaleDateString();

    url.searchParams.append("ss", ss);
    url.searchParams.append("dest_id", destId);
    url.searchParams.append("dest_type", destType);
    url.searchParams.append("checkin", checkIn);
    url.searchParams.append("checkout", checkOut);
    url.searchParams.append("group_adults", "1");
    url.searchParams.append("no_rooms", "1");
    url.searchParams.append("group_children", "0");
    url.searchParams.append("sb_travel_purpose", "leisure");
    url.searchParams.append("hpos", "1");
    url.searchParams.append("hapos", "1");

    const response = await axios.get(url, {
      headers: {
        'User-Agent': 'Your Customer Agent',
        'Content-Type': 'application/json',
      }
    });
    const $ = cheerio.load(response.data);
    const hotelUrl = $('a[data-testid="property-card-desktop-single-image"]').attr('href');
    const urlPart = hotelUrl.split('.html')[0] + '.html';
    res.json(urlPart);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
});

function validateScrapperTaskPayload(obj) {
  const schema = Joi.object({
    ss: Joi.string().required(),
    destId: Joi.string().required(),
    destType: Joi.string().required()
  });

  const {error, value} = schema.validate(obj);

  // if (error) {
  //   throw new ValidationErrorScrapperPayloadError(`Validation error: ${error.details[0].message}`);
  // }

  return value;
}

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
