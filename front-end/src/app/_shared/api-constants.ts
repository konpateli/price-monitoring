export const login: string = '/login';
export const register: string = '/register';
export const emailVerification: string = '/email/verification';
export const emailPasswordReset: string = '/password/reset';
export const resetPassword: string = '/password';
