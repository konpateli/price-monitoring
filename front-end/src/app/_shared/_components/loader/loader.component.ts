import {Component} from '@angular/core';
import {LoaderService} from "../../service/loader.service";
import {Observable} from "rxjs";

@Component({
  selector: 'pm-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  isUserLoading$: Observable<boolean> = this.loaderService.isUserLoadingSource.asObservable();

  constructor(private loaderService: LoaderService) {
  }
}
