import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

export interface QuestionDialogDto {
  message: string;
  approveButton: string;
  rejectButton: string
}
@Component({
  selector: 'pm-question-dialog',
  templateUrl: './question-dialog.component.html',
  styleUrl: './question-dialog.component.scss'
})
export class QuestionDialogComponent {

  constructor(private dialogRef: MatDialogRef<QuestionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: QuestionDialogDto) {
  }

  reject(): void {
    this.dialogRef.close(false);
  }

  approve(): void {
    this.dialogRef.close(true);
  }
}
