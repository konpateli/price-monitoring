import {Injectable} from '@angular/core';
import {MatSidenav} from "@angular/material/sidenav";

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  private leftNav: MatSidenav | any;

  public setSidenav(leftNav: MatSidenav): void {
    this.leftNav = leftNav;
  }

  public toggleSideNav(): void {
    this.leftNav?.toggle();
  }
}
