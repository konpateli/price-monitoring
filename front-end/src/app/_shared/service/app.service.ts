import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {MonitorListDetails} from "../interfaces/monitor-list-info";

@Injectable({
  providedIn: 'root'
})
export class AppService {
  isLoggedInSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  createMonitorListSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  addRoomsSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  updateTitleSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  monitorListIdToUpdate: BehaviorSubject<number | null> = new BehaviorSubject<number | null>(null);
  monitorListToUpdateSubject: BehaviorSubject<MonitorListDetails | null> = new BehaviorSubject<MonitorListDetails | null>(null);
  isDesktopSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  activeLinkSource: BehaviorSubject<string> = new BehaviorSubject<string>('booking');

  setLoggedIn(): void {
    if (sessionStorage.getItem('token')) {
      this.isLoggedInSource.next(true);
    } else {
      this.isLoggedInSource.next(false);
    }
  }

  isLoggedIn(): boolean {
    return !!sessionStorage.getItem('token');
  }

  isDesktop(): void {
    const ua: string = navigator.userAgent;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)) {
      this.isDesktopSubject.next(false);
    } else {
      this.isDesktopSubject.next(true);
    }
  }
}
