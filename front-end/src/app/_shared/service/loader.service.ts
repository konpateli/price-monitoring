import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  isUserLoadingSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isLoadingSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }
}
