import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private renderer: Renderer2;
  isDarkModeSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private colorTheme!: string;

  constructor(rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  initTheme() {
    this.getColorTheme();
    this.renderer.addClass(document.body, this.colorTheme);
  }

  toggleDarkTheme(theme: 'dark-mode' | 'light-mode') {
    this.setColorTheme(theme);
    if (theme === 'light-mode') {
      this.renderer.removeClass(document.body, 'dark-mode');
      this.isDarkModeSource.next(false);
    } else {
      this.renderer.addClass(document.body, theme);
      this.isDarkModeSource.next(true);
    }
  }

  isDarkMode() {
    return this.colorTheme === 'dark-mode';
  }

  private setColorTheme(theme: string) {
    this.colorTheme = theme;
    localStorage.setItem('pm-theme', theme);
  }

  private getColorTheme() {
    if (localStorage.getItem('pm-theme')) {
      this.colorTheme = localStorage.getItem('pm-theme') as string;
    }
  }
}
