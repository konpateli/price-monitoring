import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {HotelInfo} from "../interfaces/monitor-list-create";

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  subscriptionUrl: string = environment.baseUrl + '/subscription';

  constructor(private http: HttpClient) {
  }

  subscribe(type: string): Observable<HotelInfo> {
    return this.http.post<any>(this.subscriptionUrl + '/' + type, {}, {responseType: 'text' as 'json'});
  }
}
