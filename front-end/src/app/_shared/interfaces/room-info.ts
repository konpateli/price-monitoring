export interface RoomInfo {
  roomID: number;
  description: string;
  name: string;
  statistics: RoomStatistics;
  hotelID: number;
  hotelName: string;
  attributes: string[];
  checked: boolean;
}

export interface RoomStatistics {
  avg: number;
  min: number;
  max: number;
}

export interface Hotel {
  name: string;
  description: string;
  location: Location;
  url: string;
  hotelType: string;
  score: string;
  hotelID: number;
  rooms: Room[]
}

export interface Location {
  address: string;
  city: string;
  longitude: number;
  latitude: number;
}

export interface Room {
  roomID: number;
  description: string;
  name: string;
  hotelID: number;
  hotelName: string;
  attributes: string[];
}
