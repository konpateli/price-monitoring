export interface RoomPriceHistory {
  _embedded: Embedeed;
  _links: LinkList;
  page: Page;
}

export interface LinkList {
  first: Link;
  self: Link;
  next: Link;
  last: Link;
}

export interface Link {
  href: string;
}

export interface Embedeed {
  priceInfoList: PriceInfo[];
}

export interface Page {
  size: number;
  totalElements: number;
  totalPages: number;
  number: number;
}

export interface PriceInfo {
  id: number,
  sleeps: number,
  price: number,
  quantity: number,
  distanceDays: number,
  cancellationPolicy: string,
  timestamp: string,
  breakfastPolicy: string,
  attributes: string[]
}

export interface Statistic {
  roomId: number;
  roomName: string;
  priceInfoList?: PriceInfo[];
}
