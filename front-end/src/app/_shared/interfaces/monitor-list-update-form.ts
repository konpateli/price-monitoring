export interface MonitorListUpdateForm {
  rooms: number[];
  name: string;
  id: number;
}
