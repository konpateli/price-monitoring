export interface MonitorListCreateRequest {
  name: string;
  rooms: number[];
  distanceDays: number[];
}

export interface MonitorListUpdateRequest extends MonitorListCreateRequest {
  id: number;
}
