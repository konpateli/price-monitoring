import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {Destination} from "./booking-autocomplete-destinations";
import {Location} from "./location";
import {RoomInfo} from "./room-info";

export interface HotelInfo {
  name: string;
  description: string;
  location: Location;
  url: string;
  hotelType: string;
  score: number | null;
  hotelID: number;
  allChecked: boolean;
  expandable: boolean;
  rooms: RoomInfo[];
}

export interface GetHotelInfo {
  name: string | null;
  checkin: Date | null;
  distances: number[];
}

export interface RoomSelect {
  destination: Destination | null;
  checkin: Date | null;
  checkout: Date | null;
}

export const GetHotelInfoFormGroup = new FormGroup({
  name: new FormControl<string | null>(null, [Validators.required]),
  monitoringDates: new FormControl<Date | null>(null, [Validators.required])
});

export const RoomSelectFormGroup = new FormGroup<Record<keyof RoomSelect, AbstractControl>>({
  destination: new FormControl<Destination | null>(null),
  checkin: new FormControl<Date | null>(null, [Validators.required]),
  checkout: new FormControl<Date | null>(null, [Validators.required])
});
