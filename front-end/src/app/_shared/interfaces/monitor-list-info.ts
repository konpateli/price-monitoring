import {RoomInfo} from "./room-info";

export interface MonitorListInfo {
  monitorListName: string;
  monitorListID: number;
}

export interface MonitorListDetails {
  monitorListName: string;
  monitorListID: number;
  monitorListStatistics: MonitorListStatistics;
  distances: number[];
  rooms: RoomInfo[];
}

export interface MonitorList {
  monitorListName: string;
  monitorListID: number;
  monitorListStatistics: MonitorListStatistics;
  distances: number[];
  hotels: Map<string, RoomInfo[]>;
}

export interface MonitorListStatistics {
  avg: number;
  min: number;
  max: number;
}
