export interface Customer {
  jwt?: string;
  email?: string;
  name?: string;
  password: string;
  repeatedPassword?: string;
}

export interface ResetPassword {
  jwt: string;
  password: string;
  repeatedPassword: string;
}

export interface EmailData {
  email: string;
}
