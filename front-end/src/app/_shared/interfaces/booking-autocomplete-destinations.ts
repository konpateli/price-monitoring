import {HotelInfo} from "./monitor-list-create";

export interface BookingAutocompletePayload {
  query: string | null;
  pageview_id?: string | null;
  aid?: number | null;
  language?: string | null;
  size: number;
}

export interface BMaxLosData {
  defaultLos: number;
  extendedLos: number;
  experiment: string;
  fullOn: boolean;
}

export interface Label {
  text: string;
  precomposed: string;
  hl: number;
  dest_type: string;
  dest_id: string;
}

export interface Destination {
  b_max_los_data: BMaxLosData;
  b_show_entire_homes_checkbox: boolean;
  cc1: string;
  cjk: boolean;
  dest_id: string;
  dest_type: string;
  label: string;
  label1: string;
  label2: string;
  labels: Label[];
  latitude: number;
  lc: string;
  longitude: number;
  nr_homes: number;
  nr_hotels: number;
  nr_hotels_25: number;
  photo_uri: string;
  roundtrip: string;
  rtl: boolean;
  value: string;
  url: string;
  // hotelInfo: HotelInfo;
}

export interface BookingAutocompleteResult {
  results: Destination[];
}

export interface HotelScrapperPayload {
  ss: string;
  destId: string;
  destType: string;
}
