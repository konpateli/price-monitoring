export interface Location {
  address: string;
  city: string;
  longitude: number;
  latitude: number;
  postalCode: string;
  fullAddress: string;
}
