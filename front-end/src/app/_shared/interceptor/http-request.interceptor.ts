import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoaderService} from "../service/loader.service";
import {tap} from 'rxjs/operators';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  requestCounter: number = 0;

  constructor(private loaderService: LoaderService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!request.url.includes('/autocomplete')) {
      if (request.url.includes('/user')) {
        this.loaderService.isUserLoadingSource.next(true);
      } else {
        this.loaderService.isLoadingSource.next(true);
      }
      this.requestCounter += 1;
    }
    return next.handle(request)
      .pipe(
        tap({
          next: (event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              this.requestCounter -= 1;
              if (this.requestCounter === 0) {
                this.loaderService.isUserLoadingSource.next(false);
                this.loaderService.isLoadingSource.next(false);
              }
              if (request.url?.includes('login') || request.url?.includes('subscription')) {
                const jwt: string | null = event.body;
                if (jwt) {
                  sessionStorage.setItem('token', jwt);
                }
              }
            }
          },
          error: () => {
            this.requestCounter -= 1;
            if (this.requestCounter === 0) {
              this.loaderService.isUserLoadingSource.next(false);
              this.loaderService.isLoadingSource.next(false);
            }
          }
        }));
  }
}
