import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken: string | null = sessionStorage.getItem('token');
    if (authToken && !request.url.includes('/autocomplete')) {
      const authRequest: HttpRequest<any> = request.clone({
        setHeaders: {'Authorization': 'Bearer ' + authToken}
      });
      return next.handle(authRequest);
    }
    return next.handle(request);
  }
}
