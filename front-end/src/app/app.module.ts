import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatTabsModule} from "@angular/material/tabs";
import {MatToolbarModule} from "@angular/material/toolbar";
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {SheetComponent} from './_shared/_components/sheet/sheet.component';
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {HttpRequestInterceptor} from "./_shared/interceptor/http-request.interceptor";
import {LoaderComponent} from './_shared/_components/loader/loader.component';
import {JwtInterceptor} from "./_shared/interceptor/jwt.interceptor";
import {OffLineModeInterceptor} from "./_shared/interceptor/off-line-mode.interceptor";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {ToastrModule} from "ngx-toastr";
import {MatMenuModule} from "@angular/material/menu";
import {MatTooltipModule} from "@angular/material/tooltip";
import {QuestionDialogComponent} from "./_shared/_components/question-dialog/question-dialog.component";
import {SubscriptionComponent} from "./main/subscription/subscription.component";

@NgModule({
  declarations: [
    AppComponent,
    SheetComponent,
    LoaderComponent,
    QuestionDialogComponent,
    SubscriptionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatSidenavModule,
    MatTabsModule,
    MatExpansionModule,
    ToastrModule.forRoot(),
    MatMenuModule,
    MatTooltipModule
  ],
  providers: [MatBottomSheet,
    {provide: MAT_DATE_LOCALE, useValue: 'el-GR'},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: OffLineModeInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
