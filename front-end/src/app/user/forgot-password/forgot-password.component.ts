import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {EmailData} from "../../_shared/interfaces/customer";
import {CustomerService} from "../_shared/service/customer.service";
import {ToastService} from "../../_shared/service/toast.service";
import {Router} from "@angular/router";

@Component({
  selector: 'pm-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  form: FormGroup;
  typedEmail: boolean = false;

  constructor(private fb: FormBuilder,
              private userService: CustomerService,
              private toastService: ToastService,
              private router: Router) {
    this.form = this.fb.group({
      email: this.fb.control<string | null>(
        {value: '', disabled: false},
        {nonNullable: true, validators: [Validators.required]}
      )
    });
  }

  sendEmail(): void {
    if (this.Email.value) {
      const input: EmailData = {
        email: this.Email.value?.trim()
      };
      this.userService.sendEmailPasswordReset(input).subscribe({
        next: () => {
          this.router.navigate(['reset-password']).then(() =>
            this.toastService.showSuccess('A token has been sent to your email'));
        },
        error: (error) => {
          if (error.status === 429) {
            this.toastService.showError('Please check your email! A token has already been sent to you');
          } else {
            this.toastService.showError('There was a problem! Please try later');
          }
        }
      })
    }
  }

  get Email(): FormControl<string> {
    return this.form.get('email') as FormControl;
  }
}
