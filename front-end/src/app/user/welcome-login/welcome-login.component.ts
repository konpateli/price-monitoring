import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {AppService} from "../../_shared/service/app.service";

@Component({
  selector: 'pm-welcome-login',
  templateUrl: './welcome-login.component.html',
  styleUrls: ['./welcome-login.component.scss']
})
export class WelcomeLoginComponent {
  isDesktop$: Observable<boolean> = this.appService.isDesktopSubject.asObservable();

  constructor(private appService: AppService) {
  }
}
