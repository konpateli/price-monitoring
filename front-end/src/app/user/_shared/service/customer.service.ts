import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Customer, EmailData, ResetPassword} from "../../../_shared/interfaces/customer";
import {emailPasswordReset, emailVerification, login, register, resetPassword} from "../../../_shared/api-constants";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private customerUrl: string = environment.baseUrl + '/customer';

  constructor(private http: HttpClient) {
  }

  sendEmailVerification(input: EmailData): Observable<any> {
    return this.http.post<Response>(this.customerUrl + emailVerification, input);
  }

  signUp(input: Customer): Observable<any> {
    return this.http.post<Response>(this.customerUrl + register, input);
  }

  login(input: Customer): Observable<string> {
    return this.http.post<string>(this.customerUrl + login, input, {responseType: 'text' as 'json'});
  }

  sendEmailPasswordReset(input: EmailData): Observable<any> {
    return this.http.post<Response>(this.customerUrl + emailPasswordReset, input);
  }

  resetPassword(input: ResetPassword): Observable<any> {
    return this.http.post<Response>(this.customerUrl + resetPassword, input);
  }
}
