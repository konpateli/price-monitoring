import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';

@Directive({
  selector: '[pmPasswordValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: RepeatPasswordDirective,
      multi: true,
    },
  ],
})
export class RepeatPasswordDirective implements Validator {
  @Input('pmPasswordValidator') repeatPasswordControlName!: string;

  validate(control: AbstractControl): ValidationErrors | null {
    if (!this.repeatPasswordControlName) {
      throw new Error('pmPasswordValidator directive must receive a control name as input.');
    }

    const repeatPasswordControl = control.root.get(this.repeatPasswordControlName);

    if (!repeatPasswordControl) {
      throw new Error(`Control with name ${this.repeatPasswordControlName} not found in form.`);
    }

    const password = control.value;
    const confirmedPassword = repeatPasswordControl.value;

    if (password && confirmedPassword && password !== confirmedPassword) {
      return { passwordMismatch: true };
    }

    return null;
  }
}
