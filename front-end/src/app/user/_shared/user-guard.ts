import {CanActivateFn, Router} from "@angular/router";
import {inject} from "@angular/core";
import {AppService} from "../../_shared/service/app.service";

export const userGuard: CanActivateFn = (): boolean => {
  const appService: AppService = inject(AppService);
  const router: Router = inject(Router);
  if (appService.isLoggedIn()) {
    router.navigate(['main']);
    return false;
  } else {
    return true;
  }
}
