import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomerService} from "../_shared/service/customer.service";
import {ToastService} from "../../_shared/service/toast.service";
import {EmailData} from "../../_shared/interfaces/customer";
import {EmailValidatorDirective} from "../_shared/directives/email-validator.directive";
import {Router} from "@angular/router";

@Component({
  selector: 'pm-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  providers: [{provide: EmailValidatorDirective}]
})
export class SignUpComponent {
  form: FormGroup;
  error: string | undefined;

  constructor(private fb: FormBuilder,
              private userService: CustomerService,
              private toastService: ToastService,
              private router: Router
  ) {
    this.form = this.fb.group({
      email: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required, Validators.email]}
      )
    });
  }

  sendEmailVerification(): void {
    if (this.Email.value) {
      const input: EmailData = {
        email: this.Email.value?.trim()
      };
      this.userService.sendEmailVerification(input).subscribe({
        next: () => {
          this.router.navigate(['register']).then(() =>
            this.toastService.showSuccess('A token has been sent to your email'));
        },
        error: (error) => {
          if (error.status === 429) {
            this.toastService.showError('Please check your email! A token has already been sent to you');
          } else {
            this.toastService.showError('There was a problem! Please try later');
          }
        }
      });
    }
  }

  get Email(): FormControl<string> {
    return this.form.get('email') as FormControl;
  }
}
