import{NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {LoginComponent} from "./login/login.component";
import {SignUpComponent} from "./sign-up/sign-up.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {MatCardModule} from "@angular/material/card";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {WelcomeSignUpComponent} from "./welcome-sign-up/welcome-sign-up.component";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {WelcomeLoginComponent} from './welcome-login/welcome-login.component';
import {HttpClientModule} from "@angular/common/http";
import {EmailValidatorDirective} from "./_shared/directives/email-validator.directive";
import {PasswordValidatorDirective} from './_shared/directives/password-validator.directive';
import {RegisterComponent} from "./register/register.component";
import {RepeatPasswordDirective} from "./_shared/directives/repeat-password.directive";
import {MatProgressSpinner} from "@angular/material/progress-spinner";


@NgModule({
  declarations: [
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    WelcomeSignUpComponent,
    ResetPasswordComponent,
    WelcomeLoginComponent,
    RegisterComponent,
    EmailValidatorDirective,
    PasswordValidatorDirective,
    RepeatPasswordDirective
  ],
    imports: [
        CommonModule,
        HttpClientModule,
        UserRoutingModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatProgressSpinner
    ]
})
export class UserModule {
}
