import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomerService} from "../_shared/service/customer.service";
import {ToastService} from "../../_shared/service/toast.service";
import {Customer} from "../../_shared/interfaces/customer";
import {PasswordValidatorDirective} from "../_shared/directives/password-validator.directive";
import {Router} from "@angular/router";

@Component({
  selector: 'pm-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss',
  providers: [{provide: PasswordValidatorDirective}]
})
export class RegisterComponent {
  form: FormGroup;
  hide: boolean = true;
  hide2: boolean = true;
  error: string | undefined;

  constructor(private fb: FormBuilder,
              private userService: CustomerService,
              private toastService: ToastService,
              private router: Router
  ) {
    this.form = this.fb.group({
      jwt: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required, Validators.email]}
      ),
      name: this.fb.control<string | null>(
        {value: '', disabled: false},
        {nonNullable: true, validators: [Validators.required]}
      ),
      password: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required]}
      ),
      repeatedPassword: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required]}
      )
    });
  }

  signUp(): void {
    if (this.Fullname.value && this.Password.value && this.RepeatedPassword && this.Token.value) {
      const input: Customer = {
        name: this.Fullname.value?.trim(),
        password: this.Password.value?.trim(),
        repeatedPassword: this.RepeatedPassword.value?.trim(),
        jwt: this.Token.value?.trim()
      };
      this.userService.signUp(input).subscribe({
        next: () => {
          this.router.navigate(['login']).then(() =>
            this.toastService.showSuccess('Your registration has been completed successfully!'));
        }, error: (error: any) => {
          if (error.status === 409) {
            this.error = 'A user with this email already exists'
          } else if (error.status === 401) {
            this.error = 'Invalid token'
          } else {
            this.toastService.showError('There was a problem! Please try later');
          }
        }
      });
    }
  }

  get Token(): FormControl<string> {
    return this.form.get('jwt') as FormControl;
  }

  get Fullname(): FormControl<string> {
    return this.form.get('name') as FormControl;
  }

  get Password(): FormControl<string> {
    return this.form.get('password') as FormControl;
  }

  get RepeatedPassword(): FormControl<string> {
    return this.form.get('repeatedPassword') as FormControl;
  }
}
