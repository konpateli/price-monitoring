import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Customer} from "../../_shared/interfaces/customer";
import {AppService} from "../../_shared/service/app.service";
import {CustomerService} from "../_shared/service/customer.service";
import {ToastService} from "../../_shared/service/toast.service";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'pm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form: FormGroup;
  hide: boolean = true;
  error: string | undefined;
  isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private fb: FormBuilder,
              private router: Router,
              private appService: AppService,
              private userService: CustomerService,
              private toastService: ToastService) {
    this.form = this.fb.group({
      email: this.fb.control<string | null>(
        {value: null, disabled: false}, {validators: [Validators.required]}
      ),
      password: this.fb.control<string | null>(
        {value: null, disabled: false}, {validators: [Validators.required]}
      )
    });
  }

  login(): void {
    if (this.Email.value && this.Password.value) {
      this.isLoading.next(true);
      const input: Customer = {
        email: this.Email.value?.trim(),
        password: this.Password.value?.trim()
      };
      this.userService.login(input).subscribe({
        next: () => {
          this.appService.setLoggedIn();
          this.router.navigate(['monitor']).then(() => {
            this.toastService.showSuccess('Hello! You are logged in');
            this.isLoading.next(false);
          });
        }, error: (error) => {
          this.isLoading.next(false);
          if (error.status === 401) {
            this.error = 'Invalid email or password';
          } else {
            this.toastService.showError('There was a problem! Please try later');
          }
        }
      });
    }
  }

  get Email(): FormControl<string> {
    return this.form.get('email') as FormControl;
  }

  get Password(): FormControl<string> {
    return this.form.get('password') as FormControl;
  }
}
