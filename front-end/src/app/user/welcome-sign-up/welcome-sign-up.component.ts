import { Component } from '@angular/core';
import {Observable} from "rxjs";
import {AppService} from "../../_shared/service/app.service";

@Component({
  selector: 'pm-welcome-sign-up',
  templateUrl: './welcome-sign-up.component.html',
  styleUrls: ['./welcome-sign-up.component.scss']
})
export class WelcomeSignUpComponent {
  isDesktop$: Observable<boolean> = this.appService.isDesktopSubject.asObservable();

  constructor(private appService: AppService) {
  }
}
