import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomerService} from "../_shared/service/customer.service";
import {ResetPassword} from "../../_shared/interfaces/customer";
import {ToastService} from "../../_shared/service/toast.service";
import {Router} from "@angular/router";

@Component({
  selector: 'pm-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {
  form: FormGroup;
  hide: boolean = true;
  hide2: boolean = true;
  error: string | undefined;

  constructor(private fb: FormBuilder,
              private userService: CustomerService,
              private toastService: ToastService,
              private router: Router) {
    this.form = this.fb.group({
      jwt: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required, Validators.email]}
      ),
      password: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required]}
      ),
      repeatedPassword: this.fb.control<string | null>(
        {value: null, disabled: false},
        {nonNullable: true, validators: [Validators.required]}
      )
    });
  }

  resetPassword(): void {
    if (this.Token.value && this.Password.value && this.RepeatedPassword) {
      const input: ResetPassword = {
        jwt: this.Token.value?.trim(),
        password: this.Password.value?.trim(),
        repeatedPassword: this.RepeatedPassword.value?.trim()
      };
      this.userService.resetPassword(input).subscribe({
        next: () => {
          this.router.navigate(['login']).then(() =>
            this.toastService.showSuccess('Your password has been changed successfully'));
        }, error: (error) => {
          if (error.status === 401) {
            this.error = 'Invalid token'
          } else {
            this.toastService.showError('There was a problem! Please try later');
          }
        }
      });
    }
  }

  get Token(): FormControl<string> {
    return this.form.get('jwt') as FormControl;
  }

  get Password(): FormControl<string> {
    return this.form.get('password') as FormControl;
  }

  get RepeatedPassword(): FormControl<string> {
    return this.form.get('repeatedPassword') as FormControl;
  }
}
