import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {mainGuard} from "./main/_shared/guards/main.guard";
import {SubscriptionComponent} from "./main/subscription/subscription.component";

const routes: Routes = [
  {path: 'auth', loadChildren: () => import('./user/user.module').then(m => m.UserModule)},
  {
    path: 'monitor',
    loadChildren: () => import('./main/monitor.module').then(m => m.MonitorModule),
    canActivate: [mainGuard]
  },
  {path: 'subscription', component: SubscriptionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
