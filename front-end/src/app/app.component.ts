import {AfterViewInit, Component} from '@angular/core';
import {BehaviorSubject, filter, Observable, tap} from "rxjs";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {AppService} from "./_shared/service/app.service";
import {ThemeService} from "./_shared/service/theme.service";
import {SidenavService} from "./_shared/service/sidenav.service";
import {MainService} from "./main/_shared/services/main.service";
import {MonitorListDetails} from "./_shared/interfaces/monitor-list-info";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  isLoggedIn$: Observable<boolean> = this.appService.isLoggedInSource.asObservable();
  isDark$: Observable<boolean> = this.themeService.isDarkModeSource.asObservable();
  isMainUrl: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  activeMonitorList: Observable<MonitorListDetails | null> = this.mainService.monitorList$.asObservable();

  constructor(private appService: AppService,
              private router: Router,
              private route: ActivatedRoute,
              private mainService: MainService,
              private themeService: ThemeService,
              private sidenavService: SidenavService
  ) {
    this.appService.setLoggedIn();
    this.appService.isDesktop();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.includes('monitor')) {
          this.isMainUrl.next(true);
        } else {
          this.isMainUrl.next(false);
        }
      }
    });
  }

  ngAfterViewInit(): void {
    const theme: string = localStorage.getItem('pm-theme') || 'light-mode';
    if (theme === 'dark-mode') {
      this.themeService.initTheme();
    }
  }

  public toggleSideNav(): void {
    this.sidenavService.toggleSideNav();
  }

  subscribe(): void {
    this.mainService.monitorList$.next(null);
    this.mainService.monitorListId$.next(null);
    this.router.navigate(['../subscription']);
  }

  navigateToLists(): void {
    if (this.appService.isLoggedInSource.value) {
      this.router.navigate(['monitor']);
    }
  }

  signOut(): void {
    const authToken: string | null = sessionStorage.getItem('token');
    if (authToken) {
      sessionStorage.removeItem('token');
      this.appService.isLoggedInSource.next(false);
      this.mainService.clearMainService();
      this.router.navigate(['auth/login'], {relativeTo: this.route});
    }
  }

  /** Αλλάζει το θέμα απο λευκό σε σκοτεινό */
  toggleDarkTheme(): void {
    this.themeService.isDarkMode() ? this.themeService.toggleDarkTheme('light-mode') : this.themeService.toggleDarkTheme('dark-mode');
  }

}
