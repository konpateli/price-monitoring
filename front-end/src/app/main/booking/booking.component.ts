import {Component} from '@angular/core';
import {MainService} from "../_shared/services/main.service";
import {BehaviorSubject} from "rxjs";
import {SafeResourceUrl} from "@angular/platform-browser";

@Component({
  selector: 'pm-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent {
  urlSource: BehaviorSubject<SafeResourceUrl | null> = new BehaviorSubject<SafeResourceUrl | null>(null);

  constructor(private mainService: MainService) {
    this.mainService.bookingUrlSource.subscribe((url: string) => {
    });
  }
}
