import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject} from "rxjs";
import {DatePipe} from "@angular/common";
import {HotelInfo, RoomSelectFormGroup} from "../../_shared/interfaces/monitor-list-create";
import {
  BookingAutocompletePayload,
  BookingAutocompleteResult,
  Destination,
  HotelScrapperPayload
} from 'src/app/_shared/interfaces/booking-autocomplete-destinations';
import {MainService} from "../_shared/services/main.service";
import {ToastService} from "../../_shared/service/toast.service";
import {RoomInfo} from "../../_shared/interfaces/room-info";
import {MonitorListCreateRequest, MonitorListUpdateRequest} from "../../_shared/interfaces/monitor-list-create-request";
import {HotelInfoRequest} from "../../_shared/interfaces/hotel-info-request";
import {AppService} from "../../_shared/service/app.service";
import {MonitorListDetails} from "../../_shared/interfaces/monitor-list-info";


@Component({
  selector: 'pm-create-set',
  templateUrl: './create-set.component.html',
  styleUrl: './create-set.component.scss'
})
export class CreateSetComponent implements OnInit {
  firstPartCompletedSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  monitorForm: FormGroup;
  roomForm: FormGroup = RoomSelectFormGroup;
  destinationSource: BehaviorSubject<Destination[]> = new BehaviorSubject<Destination[]>([]);
  hotelInfoListSource: BehaviorSubject<HotelInfo[]> = new BehaviorSubject<HotelInfo[]>([]);
  rooms: number[] = [];

  constructor(public appService: AppService,
              private mainService: MainService,
              private datePipe: DatePipe,
              private formBuilder: FormBuilder,
              private toastService: ToastService) {
    this.monitorForm = this.formBuilder.group({
      name: new FormControl<string | null>(null, [Validators.required]),
      monitoringDates: this.formBuilder.array([])
    });
  }

  ngOnInit(): void {
    this.appService.createMonitorListSubject.subscribe((createSet: boolean) => {
      if (!createSet) {
        this.monitorForm.reset();
        this.roomForm.reset();
      } else {
        this.monitoringDates.push(this.newMonitoringDate());
        this.appService.addRoomsSubject.next(false);
        this.appService.monitorListToUpdateSubject.next(null);
      }
    });
    this.appService.addRoomsSubject.subscribe((updateSet: boolean) => {
      if (updateSet) {
        const monitorListToUpdate: MonitorListDetails | null = this.appService.monitorListToUpdateSubject.value;
        if (monitorListToUpdate) {
          this.firstPartCompletedSource.next(true);
        }
      }
    });
  }

  get monitoringDates(): FormArray {
    return this.monitorForm.get('monitoringDates') as FormArray;
  }

  newMonitoringDate(): FormGroup {
    return this.formBuilder.group({
      monitoringDate: []
    });
  }

  addMonitoringDate(): void {
    this.monitoringDates.push(this.newMonitoringDate());
  }

  removeMonitoringDate(i: number) {
    this.monitoringDates.removeAt(i);
  }

  backToSetList(): void {
    this.appService.createMonitorListSubject.next(false);
    this.appService.addRoomsSubject.next(false);
    this.appService.monitorListToUpdateSubject.next(null);
    this.appService.monitorListIdToUpdate.next(null);
  }

  createMonitorList(): void {
    if (this.monitorForm.invalid) {
      this.toastService.showError('You must complete a title for your monitor list');
      return;
    }
    if (!this.monitoringDates.controls.some((control: AbstractControl<any, any>) => control.get('monitoringDate')?.value !== null)) {
      this.toastService.showError('You must complete at least one monitoring date');
      return;
    }
    const hotelInfoList: HotelInfo[] = this.hotelInfoListSource.value;
    this.rooms = [];
    hotelInfoList.forEach((hotelInfo: HotelInfo) => {
      const hotelRooms: number[] = hotelInfo.rooms.filter((room: RoomInfo) => room.checked).map((room: RoomInfo) => room.roomID);
      this.rooms.unshift(...hotelRooms);
    });
    if (this.rooms.length === 0) {
      this.toastService.showError('You must add at least one room');
      return;
    }
    const distanceDays: number[] = [];
    this.monitoringDates.controls.forEach((control: AbstractControl<any, any>) => {
      if (control.get('monitoringDate')?.value !== null) {
        const days: number = this.calculateDaysDifference(control.value.monitoringDate);
        distanceDays.push(days);
      }
    });
    if (this.monitorForm.valid && this.rooms.length !== 0) {
      const input: MonitorListCreateRequest = {
        name: this.Name.value,
        rooms: this.rooms,
        distanceDays: distanceDays
      }
      this.mainService.createMonitorList(input).subscribe({
        next: () => {
          this.appService.createMonitorListSubject.next(false);
          this.toastService.showSuccess('New list created successfully');
          this.mainService.getAllMonitorLists().subscribe({
            error: (error: any) => {
              this.toastService.showError(error.error);
            }
          });
        }, error: () => {
          this.toastService.showError('Error creating ' + this.Name.value + ' list');
        }
      });
    }
  }

  updateMonitorList(): void {
    const hotelInfoList: HotelInfo[] = this.hotelInfoListSource.value;
    this.rooms = this.appService.monitorListToUpdateSubject.value?.rooms.flatMap((room: RoomInfo) => room.roomID)!;
    hotelInfoList.forEach((hotelInfo: HotelInfo) => {
      const hotelRooms: number[] = hotelInfo.rooms.filter((room: RoomInfo) => room.checked).map((room: RoomInfo) => room.roomID);
      this.rooms.unshift(...hotelRooms);
    });
    if (this.monitorForm.valid) {
      const input: MonitorListUpdateRequest = {
        id: this.appService.monitorListToUpdateSubject.value?.monitorListID!,
        name: this.appService.monitorListToUpdateSubject.value?.monitorListName!,
        rooms: this.rooms,
        distanceDays: this.appService.monitorListToUpdateSubject.value?.distances!
      }
      this.mainService.updateMonitorList(input).subscribe({
        next: () => {
          this.appService.addRoomsSubject.next(false);
          this.appService.monitorListToUpdateSubject.next(null);
          this.appService.monitorListIdToUpdate.next(null);
          this.toastService.showSuccess('Rooms added successfully');
          this.mainService.getAllMonitorLists().subscribe({
            error: (error: any) => {
              this.toastService.showError(error.error);
            }
          });
        }, error: () => {
          this.toastService.showError('Error updating ' + this.Name.value + ' list');
        }
      });
    }
  }

  private calculateDaysDifference(givenDate: Date): number {
    const currentDate: Date = new Date();
    currentDate.setHours(0, 0, 0, 0);
    givenDate.setHours(0, 0, 0, 0);
    const timeDifference: number = givenDate.getTime() - currentDate.getTime();
    const daysDifference: number = timeDifference / (1000 * 3600 * 24);
    return Math.floor(daysDifference);
  }

  type(event: any): void {
    const payload: BookingAutocompletePayload = {
      query: event.target.value,
      pageview_id: 'cd2e9b689b5c00fa',
      aid: 304142,
      language: 'en-gb',
      size: 4
    }
    this.mainService.getBookingDestinations(payload).subscribe({
      next: (response: BookingAutocompleteResult) => {
        const destinations: Destination[] = response.results
          .filter((result: Destination) => result.dest_type === 'hotel');
        this.destinationSource.next(destinations);
      },
      error: (error: any) => {
        console.log(error);
      }
    })
  }

  onselect(event: any): void {
    const destination: Destination = event.option.value;
    const hotelScrapperPayload: HotelScrapperPayload = {
      ss: destination.value,
      destId: destination.dest_id,
      destType: destination.dest_type
    }
    this.mainService.hotelScrapper(hotelScrapperPayload).subscribe({
      next: (response: string) => {
        if (this.roomForm.get('checkin')?.value && this.roomForm.get('checkout')?.value) {
          const checkin: Date = this.roomForm.get('checkin')?.value;
          const checkout: Date = this.roomForm.get('checkout')?.value;
          const formattedCheckin: string = this.formatDate(checkin);
          const formattedCheckout: string = this.formatDate(checkout);
          response = response + '?' + 'checkin=' + formattedCheckin + '&checkout=' + formattedCheckout;
        }
        const hotelInfoRequest: HotelInfoRequest = {link: response};
        this.mainService.hotelInfo(hotelInfoRequest).subscribe({
          next: (response: HotelInfo) => {
            const hotelInfo: HotelInfo = response;
            if (hotelInfo.rooms.length !== 0) {
              const hotelsInfoList: HotelInfo[] = this.hotelInfoListSource.value;
              hotelsInfoList.push(hotelInfo);
              this.hotelInfoListSource.next(...[hotelsInfoList]);
            } else {
              this.toastService.showError('No rooms found for accommodation ' + hotelInfo.name);
            }
          },
          error: () => {
            this.toastService.showError('Sorry, we did not manage to find rooms for this accommodation');
          }
        })
      }
    });
    this.Destination.reset();
  }

  formatDate(date: Date | null): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd') || '';
  }

  backToMonitorForm(): void {
    this.firstPartCompletedSource.next(false);
  }

  updateAllComplete(hotelInfo: HotelInfo) {
    hotelInfo.allChecked = hotelInfo.rooms != null && hotelInfo.rooms.every((roomInfo: RoomInfo) => roomInfo.checked);
  }

  someComplete(hotelInfo: HotelInfo): boolean {
    return hotelInfo.rooms.filter((roomInfo: RoomInfo) => roomInfo.checked).length > 0 && !hotelInfo.allChecked;
  }

  setAll(checked: boolean, hotelInfo: HotelInfo) {
    hotelInfo.allChecked = checked;
    if (hotelInfo.rooms.length === 0) {
      return;
    }
    hotelInfo.rooms.forEach(t => (t.checked = checked));
  }

  continue(): void {
    this.firstPartCompletedSource.next(true);
  }

  // removeDestination(destination: Destination) {
  //     let selectedDestinations: Destination[] = this.selectedDestinationSource.value;
  //     selectedDestinations.filter((selectedDestination: Destination) => destination.value !== selectedDestination.value);
  //     this.selectedDestinationSource.next(...[selectedDestinations]);
  // }

  get Name(): FormControl<string> {
    return this.monitorForm.get('name') as FormControl;
  }

  get Destination(): FormControl<Destination> {
    return this.roomForm.get('destination') as FormControl;
  }
}
