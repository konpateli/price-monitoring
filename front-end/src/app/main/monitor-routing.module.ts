import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MonitorComponent} from "./monitor.component";
import {SubscriptionComponent} from "./subscription/subscription.component";
import {getMonitorListsResolver} from "./_shared/resolvers/resolver";
import {StatisticsComponent} from "./statistics/statistics.component";
import {BookingComponent} from "./booking/booking.component";
import {DetailsComponent} from "./details/details.component";
import {NotificationsComponent} from "./notifications/notifications.component";

const routes: Routes = [
  {path: '', component: MonitorComponent, resolve: {monitorLists: getMonitorListsResolver}},
  {
    path: ':id', component: MonitorComponent, resolve: {monitorLists: getMonitorListsResolver},
    children: [
      {path: 'booking', component: BookingComponent},
      {path: 'statistics', component: StatisticsComponent},
      {path: 'details', component: DetailsComponent},
      {path: 'notifications', component: NotificationsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitorRoutingModule {
}
