import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import {MonitorRoutingModule} from './monitor-routing.module';
import {MatTabNav, MatTabsModule} from "@angular/material/tabs";
import {MatExpansionModule} from "@angular/material/expansion";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatDividerModule} from "@angular/material/divider";
import {MatChipsModule} from "@angular/material/chips";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule, MatOptionModule, provideNativeDateAdapter} from "@angular/material/core";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTreeModule} from "@angular/material/tree";
import {StatisticsComponent} from "./statistics/statistics.component";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {LineChartComponent} from "./line-chart/line-chart.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {BookingComponent} from "./booking/booking.component";
import {SetListComponent} from "./set-list/set-list.component";
import {CreateSetComponent} from "./create-set/create-set.component";
import {MonitorComponent} from "./monitor.component";
import {MatMenuModule} from "@angular/material/menu";
import {VerticalBarComponent} from "./vertical-bar/vertical-bar.component";
import {MonthConverterPipe} from "./_shared/pipes/month-converter.pipe";
import {MatSelect} from "@angular/material/select";
import {NotificationsComponent} from "./notifications/notifications.component";
import {DetailsComponent} from "./details/details.component";
import {MatToolbarRow} from "@angular/material/toolbar";
import {SkeletonDirective} from "../_shared/skeleton.directive";


@NgModule({
  declarations: [
    MonitorComponent,
    BookingComponent,
    StatisticsComponent,
    SetListComponent,
    CreateSetComponent,
    MonitorComponent,
    VerticalBarComponent,
    LineChartComponent,
    NotificationsComponent,
    DetailsComponent,
    SkeletonDirective
  ],
  imports: [
    CommonModule,
    MonitorRoutingModule,
    MatTabsModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatChipsModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatOptionModule,
    MatCheckboxModule,
    FormsModule,
    MatTabsModule,
    MatTreeModule,
    NgxChartsModule,
    DragDropModule,
    MatMenuModule,
    MatSelect,
    MatToolbarRow
  ],
  providers: [DatePipe, MatTabNav, provideNativeDateAdapter(), MonthConverterPipe]
})
export class MonitorModule {
}
