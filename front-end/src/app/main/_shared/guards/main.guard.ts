import {CanActivateFn, Router} from "@angular/router";
import {inject} from "@angular/core";
import {AppService} from "../../../_shared/service/app.service";

export const mainGuard: CanActivateFn = (): boolean => {
    const appService: AppService = inject(AppService);
    const router: Router = inject(Router);
    if (appService.isLoggedIn()) {
        return true;
    } else {
        router.navigate(['auth/login']);
        return true;
    }
}
