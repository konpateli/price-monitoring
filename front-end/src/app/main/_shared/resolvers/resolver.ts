import {ResolveFn} from "@angular/router";
import {inject} from "@angular/core";
import {MainService} from "../services/main.service";
import {ToastService} from "../../../_shared/service/toast.service";

export const getMonitorListsResolver: ResolveFn<any> = (): void => {
  const mainService: MainService = inject(MainService);
  const toastService: ToastService = inject(ToastService);
  if (mainService.monitorListsDetailsSource.value.length === 0) {
    mainService.getAllMonitorLists().subscribe({
      error: (error) => {
        toastService.showError(error.error);
      }
    });
  }
}
