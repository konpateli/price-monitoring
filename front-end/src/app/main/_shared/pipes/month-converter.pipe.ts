import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'monthConverter',
  standalone: true
})
export class MonthConverterPipe implements PipeTransform {

  transform(value: any): string {
    const monthNumber: number = parseInt(value, 10);
    if (isNaN(monthNumber) || monthNumber < 1 || monthNumber > 12) {
      return 'Invalid Month';
    }
    const months: string[] = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return months[monthNumber - 1];
  }
}
