import {Injectable} from '@angular/core';
import {BehaviorSubject, catchError, forkJoin, Observable, retry, switchMap, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {bookingUrl} from "../main-constants";
import {MonitorList, MonitorListDetails, MonitorListInfo} from "../../../_shared/interfaces/monitor-list-info";
import {
  BookingAutocompletePayload,
  HotelScrapperPayload
} from "../../../_shared/interfaces/booking-autocomplete-destinations";
import {HotelInfoRequest} from "../../../_shared/interfaces/hotel-info-request";
import {HotelInfo} from "../../../_shared/interfaces/monitor-list-create";
import {MonitorListCreateRequest} from "../../../_shared/interfaces/monitor-list-create-request";
import {MonitorListUpdateForm} from "../../../_shared/interfaces/monitor-list-update-form";
import {Hotel, RoomInfo} from "../../../_shared/interfaces/room-info";
import {RoomPriceHistory, Statistic} from "../../../_shared/interfaces/room";

@Injectable({
  providedIn: 'root'
})
export class MainService {

  monitorListUrl: string = environment.baseUrl + '/monitor_list';
  hotelUrl: string = environment.baseUrl + '/hotel';
  priceUrl: string = environment.baseUrl + '/prices';

  monitorListsInfoSource: BehaviorSubject<MonitorListInfo[]> = new BehaviorSubject<MonitorListInfo[]>([]);
  monitorListsDetailsSource: BehaviorSubject<MonitorListDetails[]> = new BehaviorSubject<MonitorListDetails[]>([]);
  bookingUrlSource: BehaviorSubject<string> = new BehaviorSubject<string>(bookingUrl);
  monitorListsSource: BehaviorSubject<MonitorList[]> = new BehaviorSubject<MonitorList[]>([]);

  monitorListId$: BehaviorSubject<number | null> = new BehaviorSubject<number | null>(null);
  monitorList$: BehaviorSubject<MonitorListDetails | null> = new BehaviorSubject<MonitorListDetails | null>(null);

  roomInfoSource: BehaviorSubject<RoomInfo[]> = new BehaviorSubject<RoomInfo[]>([]);
  analyticRoomsPrices: BehaviorSubject<RoomPriceHistory[]> = new BehaviorSubject<RoomPriceHistory[]>([]);
  statisticList: BehaviorSubject<Statistic[]> = new BehaviorSubject<Statistic[]>([]);

  constructor(private http: HttpClient) {
  }

  getBookingDestinations(input: BookingAutocompletePayload): Observable<any> {
    return this.http.post<any>(environment.nodeJsUrl + '/autocomplete', input);
  }

  hotelScrapper(input: HotelScrapperPayload): Observable<any> {
    return this.http.post<any>(environment.nodeJsUrl + '/search-results', input);
  }

  hotelInfo(input: HotelInfoRequest): Observable<HotelInfo> {
    return this.http.post<any>(this.hotelUrl + '/info', input);
  }

  createMonitorList(input: MonitorListCreateRequest): Observable<Response> {
    return this.http.post<any>(this.monitorListUrl, input);
  }

  updateMonitorList(input: MonitorListUpdateForm): Observable<Response> {
    return this.http.put<any>(this.monitorListUrl, input);
  }

  deleteMonitorList(id: number): Observable<Response> {
    return this.http.delete<Response>(this.monitorListUrl + '/' + `${id}`);
  }

  getAllMonitorLists(): Observable<MonitorListInfo[]> {
    return this.http.get<MonitorListInfo[]>(this.monitorListUrl).pipe(
      switchMap((monitorListInfo: MonitorListInfo[]) => {
        this.monitorListsInfoSource.next(monitorListInfo);
        const observables: Observable<MonitorListDetails>[] = monitorListInfo.map(
          (monitorListInfo: MonitorListInfo) =>
            this.getMonitorListDetails(monitorListInfo.monitorListID)
        );
        return forkJoin(observables).pipe(
          tap((monitorListsDetails: MonitorListDetails[]) => {
            this.monitorListsDetailsSource.next(monitorListsDetails);
          }),
          catchError(() => {
            throw 'There was an error while while loading monitor lists';
          })
        )
      })
    );
  }

  getMonitorListDetails(id: number): Observable<MonitorListDetails> {
    return this.http.get<MonitorListDetails>(this.monitorListUrl + '/' + `${id}`).pipe(retry(10));
  }

  getHotel(id: number): Observable<Hotel> {
    return this.http.get<Hotel>(this.hotelUrl + '/' + `${id}`);
  }

  createHotelMap(monitorListDetails: MonitorListDetails[]): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.monitorListsSource.next([]);
      monitorListDetails.forEach((monitorDetails: MonitorListDetails) => {
        const hotelMap: Map<string, RoomInfo[]> = new Map();
        monitorDetails.rooms.forEach((roomInfo: RoomInfo) => {
          const rooms: RoomInfo[] = hotelMap.get(roomInfo.hotelName) || [];
          rooms.push(roomInfo);
          hotelMap.set(roomInfo.hotelName, rooms);
        });
        const monitorList: MonitorList = {
          monitorListID: monitorDetails.monitorListID,
          monitorListName: monitorDetails.monitorListName,
          monitorListStatistics: monitorDetails.monitorListStatistics,
          distances: monitorDetails.distances,
          hotels: hotelMap
        };
        const monitorLists: MonitorList[] = this.monitorListsSource.value;
        monitorLists.push(monitorList);
        this.monitorListsSource.next(monitorLists);
      });
      setTimeout(() => {
        console.log("Async operation completed.");
        resolve();
      });
    });
  }

  getAllRoomsPrices(): void {
    // const observables: Observable<RoomPriceHistory>[] = [];
    // const statistics: Statistic[] = [];
    // this.roomInfoSource.value.forEach((roomInfo: RoomInfo) => {
    //   const statistic: Statistic = {roomId: roomInfo.roomID, roomName: roomInfo.name};
    //   statistics.push(statistic);
    //   observables.push(this.getPricesPerRoom('/' + this.monitorListId$.value + '/' + roomInfo.roomID + '?size=2000&page=0&sort=timestamp,desc'));
    // });
    // forkJoin(observables).pipe(
    //   tap((roomPriceHistoryList: RoomPriceHistory[]) => {
    //     console.log(roomPriceHistoryList);
    //     this.analyticRoomsPrices.next(roomPriceHistoryList);
    //     for (let i = 0; i < statistics.length; i++) {
    //       statistics[i].priceInfoList = roomPriceHistoryList[i]._embedded.priceInfoList;
    //     }
    //     this.statisticList.next(statistics);
    //   }),
    //   catchError(() => {
    //     throw 'There was an error while while loading monitor lists';
    //   })).subscribe();
  }

  getPricesPerRoom(url: string): Observable<RoomPriceHistory> {
    return this.http.get<RoomPriceHistory>(this.priceUrl + url).pipe(retry(20));
  }

  clearMainService(): void {
    this.monitorListsInfoSource.next([]);
    this.monitorListsDetailsSource.next([]);
    this.monitorListsSource.next([]);
    this.monitorListId$.next(null);
    this.monitorList$.next(null);
  }
}
