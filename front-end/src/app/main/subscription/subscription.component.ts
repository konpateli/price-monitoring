import {Component} from '@angular/core';
import {SubscriptionService} from "../../_shared/service/subscription.service";
import {AppService} from "../../_shared/service/app.service";
import {ToastService} from "../../_shared/service/toast.service";
import {Router} from "@angular/router";

@Component({
  selector: 'pm-subscription',
  templateUrl: './subscription.component.html',
  styleUrl: './subscription.component.scss'
})
export class SubscriptionComponent {

  constructor(private subscriptionService: SubscriptionService,
              private appService: AppService,
              private toastService: ToastService,
              private router: Router) {
  }

  subscribe(type: string): void {
    this.subscriptionService.subscribe(type).subscribe({
      next: () => {
        this.appService.setLoggedIn();
        this.toastService.showSuccess('Your subscription change to ' + type);
        this.router.navigate(['monitor']);
      }, error: (error) => {
        if (error.status === 401 || error.status === 403) {
          this.toastService.showError('You are not allowed to change your subscription');
        } else {
          this.toastService.showError('There was a problem! Please try later');
        }
      }
    });
  }
}
