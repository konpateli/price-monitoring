import {Component} from '@angular/core';
import {Chart, Series} from "../../_shared/interfaces/charts";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {BehaviorSubject, Observable} from "rxjs";
import {MainService} from "../_shared/services/main.service";
import {RoomInfo} from "../../_shared/interfaces/room-info";
import {PriceInfo, RoomPriceHistory} from "../../_shared/interfaces/room";
import {LoaderService} from "../../_shared/service/loader.service";

@Component({
  selector: 'pm-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrl: './line-chart.component.scss'
})
export class LineChartComponent {
  isLoading$: Observable<boolean> = this.loaderService.isLoadingSource.asObservable();
  roomInfoList$: Observable<RoomInfo[]> = this.mainService.roomInfoSource.asObservable();
  form!: FormGroup;
  data: Chart[] = [];
  view: any = [900, 300];

  constructor(private mainService: MainService,
              private formBuilder: FormBuilder,
              private loaderService: LoaderService) {
    this.form = this.formBuilder.group({
      rooms: new FormControl<RoomInfo | null>(null)
    });
    this.roomInfoList$.subscribe((roomInfoList: RoomInfo[]) => {
      this.Rooms.patchValue(this.mainService.roomInfoSource.value[0]);
      this.updateLineChartData(roomInfoList[0]);
    });

    this.Rooms.valueChanges.subscribe((selectedRooms: RoomInfo | null) => {
      this.updateLineChartData(selectedRooms);
    });
  }

  private updateLineChartData(roomInfo: RoomInfo | null): void {
    const charts: Chart[] = [];
    if (roomInfo) {
      let chart: Chart = {name: roomInfo.name, series: []}
      this.mainService.getPricesPerRoom('/' + this.mainService.monitorListId$.value + '/' + roomInfo.roomID + '?size=2000&page=0&sort=timestamp,desc')
        .subscribe((roomPriceHistory: RoomPriceHistory) => {
          const priceInfoList: PriceInfo[] = roomPriceHistory._embedded.priceInfoList;
          if (priceInfoList && priceInfoList.length > 0) {
            const seriesList: Series[] = [];
            priceInfoList.forEach((priceInfo: PriceInfo) => {
              priceInfo.timestamp = priceInfo.timestamp.split('T')[0];
            });
            const separatedByTimestamp: { [timestamp: string]: PriceInfo[] } = {};
            priceInfoList.forEach((priceInfo: PriceInfo) => {
              if (!separatedByTimestamp[priceInfo.timestamp]) {
                separatedByTimestamp[priceInfo.timestamp] = [];
              }
              separatedByTimestamp[priceInfo.timestamp].push(priceInfo);
            });
            for (const timestamp in separatedByTimestamp) {
              let prices: number = 0;
              const priceInfoArray = separatedByTimestamp[timestamp];
              priceInfoArray.forEach((priceInfo: PriceInfo) => {
                prices = prices + priceInfo.price;
              });
              const avg: number = prices / priceInfoArray.length;
              const series: Series = {value: avg, name: timestamp};
              seriesList.push(series);
              chart.series = seriesList;
            }
            charts.push(chart);
            this.data = charts;
          }
        });
    }
  }

  get Rooms(): FormControl<RoomInfo> {
    return this.form.get('rooms') as FormControl<RoomInfo>;
  }
}
