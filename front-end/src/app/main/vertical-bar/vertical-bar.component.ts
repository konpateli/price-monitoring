import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {Series} from "../../_shared/interfaces/charts";
import {MainService} from "../_shared/services/main.service";
import {RoomInfo, RoomStatistics} from "../../_shared/interfaces/room-info";
import {LoaderService} from "../../_shared/service/loader.service";

@Component({
  selector: 'pm-vertical-bar',
  templateUrl: './vertical-bar.component.html',
  styleUrl: './vertical-bar.component.scss'
})
export class VerticalBarComponent implements OnInit {
  isLoading$: Observable<boolean> = this.loaderService.isLoadingSource.asObservable();
  roomInfoList$: Observable<RoomInfo[]> = this.mainService.roomInfoSource.asObservable();
  form!: FormGroup;
  data: Series[] = [];
  view: any = [900, 300];

  constructor(private mainService: MainService,
              private formBuilder: FormBuilder,
              private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      rooms: new FormControl<RoomInfo[]>([])
    });
    this.roomInfoList$.subscribe((roomInfoList: RoomInfo[]) => {
      this.Rooms.patchValue(this.mainService.roomInfoSource.value);
      this.updateVerticalBarData(roomInfoList);
    });

    this.Rooms.valueChanges.subscribe((selectedRooms: RoomInfo[] | null) => {
      this.updateVerticalBarData(selectedRooms);
    });
  }

  updateVerticalBarData(roomInfoList: RoomInfo[] | null): void {
    if (roomInfoList && roomInfoList.length > 0) {
      const seriesList: Series[] = [];
      roomInfoList.forEach((roomInfo: RoomInfo) => {
        let series: Series = {name: roomInfo.name, value: 0};
        const roomStatistics: RoomStatistics = roomInfo.statistics;
        if (roomStatistics && roomStatistics.avg) {
          series.value = roomStatistics.avg;
          seriesList.push(series);
        }
      });
      this.data = seriesList;
    }
  }

  get Rooms(): FormControl<RoomInfo[]> {
    return this.form.get('rooms') as FormControl<RoomInfo[]>;
  }
}
