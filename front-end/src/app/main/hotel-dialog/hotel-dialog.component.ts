import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'pm-hotel-dialog',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './hotel-dialog.component.html',
  styleUrl: './hotel-dialog.component.scss'
})
export class HotelDialogComponent {

}
