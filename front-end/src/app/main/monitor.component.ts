import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Observable, Subscription, tap} from "rxjs";
import {MainService} from "./_shared/services/main.service";
import {MonitorListDetails} from "../_shared/interfaces/monitor-list-info";
import {AppService} from "../_shared/service/app.service";
import {SidenavService} from "../_shared/service/sidenav.service";
import {MatSidenav} from "@angular/material/sidenav";

@Component({
  selector: 'pm-monitor-list',
  templateUrl: './monitor.component.html',
  styleUrl: './monitor.component.scss'
})
export class MonitorComponent implements AfterViewInit, OnDestroy {
  @ViewChild('sidenav') public sidenav!: MatSidenav;
  links: string[] = ['booking', 'statistics', 'details', 'notifications'];
  activeLink: string = this.links[0];
  createNewSet$: Observable<boolean> = this.appService.createMonitorListSubject.asObservable();
  monitorListsDetailsSub: Subscription;
  isDesktop$: Observable<boolean> = this.appService.isDesktopSubject.asObservable();
  monitorListId$: Observable<number | null> = this.mainService.monitorListId$.asObservable();
  addRooms$: Observable<boolean> = this.appService.addRoomsSubject.asObservable();
  createMonitorList$: Observable<boolean> = this.appService.createMonitorListSubject.asObservable();

  constructor(private route: ActivatedRoute,
              public appService: AppService,
              private sidenavService: SidenavService,
              private router: Router,
              private cd: ChangeDetectorRef,
              private mainService: MainService) {
    this.monitorListsDetailsSub = this.mainService.monitorListsDetailsSource.subscribe((monitorListDetails: MonitorListDetails[]) => {
      this.mainService.createHotelMap(monitorListDetails).then(() => {
        this.route.paramMap.pipe(
          tap((params: ParamMap) => {
            const id: number | null = +params.get('id')!;
            this.mainService.monitorListId$.next(id);
            if (id) {
              const monitorList: MonitorListDetails | null = this.mainService.monitorListsDetailsSource.value
                .find((list: MonitorListDetails) => list.monitorListID === id)!;
              this.mainService.monitorList$.next(monitorList);
              this.mainService.roomInfoSource.next(monitorList?.rooms);
              this.appService.activeLinkSource.next(this.activeLink);
              this.router.navigate([this.activeLink], {relativeTo: this.route});
            }
          })
        ).subscribe();
      });
    });
  }

  ngAfterViewInit(): void {
    if (this.sidenav) {
      this.sidenavService.setSidenav(this.sidenav);
      this.sidenav.open();
      this.cd.detectChanges();
    }
  }

  ngOnDestroy(): void {
    this.monitorListsDetailsSub.unsubscribe();
  }
}
