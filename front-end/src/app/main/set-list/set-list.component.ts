import {Component, ElementRef, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {MainService} from "../_shared/services/main.service";
import {MonitorList, MonitorListDetails} from "../../_shared/interfaces/monitor-list-info";
import {Hotel, RoomInfo} from "../../_shared/interfaces/room-info";
import {MatExpansionPanel} from "@angular/material/expansion";
import {MatDialog} from "@angular/material/dialog";
import {
  QuestionDialogComponent,
  QuestionDialogDto
} from "../../_shared/_components/question-dialog/question-dialog.component";
import {ToastService} from "../../_shared/service/toast.service";
import {AppService} from "../../_shared/service/app.service";
import {MonitorListUpdateRequest} from "../../_shared/interfaces/monitor-list-create-request";
import {LoaderService} from "../../_shared/service/loader.service";
import {Router} from "@angular/router";

@Component({
  selector: 'pm-set-list',
  templateUrl: './set-list.component.html',
  styleUrls: ['./set-list.component.scss']
})
export class SetListComponent {
  @ViewChild('panel') panel!: MatExpansionPanel;
  @ViewChild('titleField') titleField!: ElementRef;
  monitorLists$: Observable<MonitorList[]> = this.mainService.monitorListsSource.asObservable();
  isLoading$: Observable<boolean> = this.loaderService.isLoadingSource.asObservable();

  constructor(private mainService: MainService,
              public appService: AppService,
              private toastService: ToastService,
              private dialog: MatDialog,
              private router: Router,
              private loaderService: LoaderService) {
  }

  displayBooking(hotelName: string): void {
    this.mainService.monitorListsSource.value.forEach((monitorList: MonitorList) => {
      monitorList.hotels.forEach((value, key) => {
        if (key === hotelName) {
          const rooms = monitorList.hotels.get(hotelName);
          const hotelID: number = rooms![0].hotelID;
          this.mainService.getHotel(hotelID).subscribe({
            next: (hotel: Hotel) => {
              this.mainService.bookingUrlSource.next(hotel.url);
            }
          });
        }
      });
    });
  }

  addNewCompetitiveSet(): void {
    this.appService.createMonitorListSubject.next(true);
    this.mainService.monitorListId$.next(null);
    this.mainService.monitorList$.next(null);
    this.router.navigate(['monitor']);
  }

  changeListTitle(id: number): void {
    const monitorList: MonitorListDetails = this.mainService.monitorListsDetailsSource.value.find((monitorList: MonitorListDetails) => monitorList.monitorListID === id)!;
    if (monitorList) {
      this.appService.updateTitleSubject.next(true);
      this.appService.monitorListIdToUpdate.next(id);
      this.appService.monitorListToUpdateSubject.next(monitorList);
      setTimeout(() => {
        this.titleField.nativeElement.value = monitorList.monitorListName;
        this.titleField.nativeElement.focus();
      }, 0);
    }
  }

  addRooms(id: number): void {
    const monitorList: MonitorListDetails = this.mainService.monitorListsDetailsSource.value.find((monitorList: MonitorListDetails) => monitorList.monitorListID === id)!;
    if (monitorList) {
      this.appService.updateTitleSubject.next(false);
      this.appService.createMonitorListSubject.next(false);
      this.appService.addRoomsSubject.next(true);
      this.appService.monitorListIdToUpdate.next(id);
      this.appService.monitorListToUpdateSubject.next(monitorList);
    }
  }

  updateMonitorListTitle(value: string): void {
    if (value && this.appService.monitorListToUpdateSubject.value?.monitorListName !== value.trim()) {
      const rooms: number[] = this.appService.monitorListToUpdateSubject.value?.rooms.flatMap((room: RoomInfo) => room.roomID)!;
      const input: MonitorListUpdateRequest = {
        id: this.appService.monitorListToUpdateSubject.value?.monitorListID!,
        name: value,
        rooms: rooms,
        distanceDays: this.appService.monitorListToUpdateSubject.value?.distances!
      }
      this.mainService.updateMonitorList(input).subscribe({
        next: () => {
          this.appService.monitorListToUpdateSubject.next(null);
          this.appService.monitorListIdToUpdate.next(null);
          this.appService.updateTitleSubject.next(false);
          this.toastService.showSuccess('List name updated successfully');
          this.mainService.getAllMonitorLists().subscribe({
            error: (error: any) => {
              this.toastService.showError(error.error);
            }
          });
        }, error: () => {
          this.toastService.showError('Error updating the name of list');
          this.appService.updateTitleSubject.next(false);
        }
      });
    } else {
      this.appService.monitorListToUpdateSubject.next(null);
      this.appService.monitorListIdToUpdate.next(null);
      this.appService.updateTitleSubject.next(false);
    }
  }

  deleteMonitorList(id: number, name: string): void {
    const questionDialogDto: QuestionDialogDto = {
      message: 'Are you sure you want to delete ' + name + '?',
      rejectButton: 'Cancel',
      approveButton: 'Delete'
    }
    this.dialog.open(QuestionDialogComponent, {
      data: questionDialogDto
    }).afterClosed().subscribe((response: boolean) => {
      if (response) {
        this.mainService.deleteMonitorList(id).subscribe({
          next: () => {
            this.toastService.showSuccess('List ' + name + ' deleted successfully');
            this.mainService.getAllMonitorLists().subscribe({
              error: (error: any) => {
                console.log(error.error);
                this.toastService.showError(error.error);
              }
            });
          },
          error: () => {
            this.toastService.showError('Error deleting list ' + name);
          }
        });
      }
    });
  }

  deleteRoom(roomId: number, roomName: string, monitorListId: number): void {
    if (roomId && monitorListId) {
      const questionDialogDto: QuestionDialogDto = {
        message: 'Are you sure you want to delete room with name ' + roomName + '?',
        rejectButton: 'Cancel',
        approveButton: 'Delete'
      }
      this.dialog.open(QuestionDialogComponent, {
        data: questionDialogDto
      }).afterClosed().subscribe((response: boolean) => {
        if (response) {
          const monitorList: MonitorListDetails = this.mainService.monitorListsDetailsSource.value.find((monitorList: MonitorListDetails) => monitorList.monitorListID === monitorListId)!;
          const roomList: RoomInfo[] = monitorList.rooms.filter((roomInfo: RoomInfo) => roomInfo.roomID !== roomId);
          const idList: number[] = roomList.map((roomInfo: RoomInfo) => roomInfo.roomID);
          const input: MonitorListUpdateRequest = {
            id: monitorList.monitorListID,
            name: monitorList.monitorListName,
            rooms: idList,
            distanceDays: monitorList.distances
          }
          this.mainService.updateMonitorList(input).subscribe({
            next: () => {
              this.toastService.showSuccess('Room deleted successfully');
              this.mainService.getAllMonitorLists().subscribe({
                error: (error: any) => {
                  this.toastService.showError(error.error);
                }
              });
            }, error: () => {
              this.toastService.showError('Error updating the name of list');
            }
          });
        }
      });
    }
  }

  toggleMenu(event: MouseEvent): void {
    event.stopPropagation();
  }
}
